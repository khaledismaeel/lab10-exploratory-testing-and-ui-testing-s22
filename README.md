****# Lab10 - Explaratory testing and UI

## Introduction

Hey, hey, hey, what's up guys, it is almost last lab for the course here we go. Imagine your product is done, you've tested and analysed almost everything you've could, and here we have the last step in our process: UI and exploratory testing.  **Let's roll!**

## Explaratory testing

Basically, if you want to do an exploratory testing, you need to pick a use case, for example to book a ticket for "Star Wars XX: THE REVENGE OF THE VENGEANCE SKYWALKER SAGA FINAL" in a cinema, and you need to describe path you need to walk through to get what you want, marking all errors or inconsistensies you've found in the way and document each step passed.

## UI testing

Ok, after you've done previous step and you understand what actions you need to take to achieve your target you may test your UI, to check that each critical component is in place for you to accomplish needed User Story. Usually Selenium is being used - it is a multilanguage library which allows to do Unit Testing, walk through website components and imitate human behaviour. In some kind it is an automated Exploratory testing.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab10 - Explaratory testing and UI
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab10-exploratory-testing-and-ui-testing)
2. There are a lot of different ways to do the exploratory testing, such as:
 + Landmark tour(visit the most important features, just walking through use cases)
 + Antisocial tour(break everything that moves, handle yourself as a real app user, enter anything anywhere)
 + Supermodel tour(check UI of the app on each tool, any insufficiencies with requirements or with common sense)
 + etc, there are a lot of them...
3. We are going to work only with Landmark tours, here how you do it, develop the Use Case(or pick one, if you already have them), next you should create table like this for each path you are taking to accomplish described use case:
![table image](https://i.ibb.co/DgStckd/landmark-testing.png)
4. Ok, after you've created exploratory tests, it is ledgit thought to automatize some of your testing, especially for the most used and critical pathes, like login, core functionality, etc. To do it there exists a whole family of different frameworks, we are going to use Selenium for that purpose. Selenium is a whole set of products which allows your programm to run a website/application, parse them and accomplish different activities. 
   + First, we will need to download and install/unpack webdriver, which we are going to use to imitate our browser. We are going to use Firefox driver - gecko. You can dowload latest verion [here](https://github.com/mozilla/geckodriver/releases)
   + Next, if you are losing Linux, we need to add our webdriver to the system paths, you may do it like this:
```sh
export PATH=$PATH:_path_to_webdriver_
``` 
   + Ok, we are done with preps, now we can start the fun part, let's create maven project like the previous time and let's add this to our `pom.xml`:
```xml
    <properties>
        <java.version>13</java.version>
        <maven.compiler.source>13</maven.compiler.source>
        <maven.compiler.target>13</maven.compiler.target>
        <testng.version>6.8.7</testng.version>
        <selenium.version>2.39.0</selenium.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>7.1.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>3.141.5</version>
        </dependency>
    </dependencies>
```
   + That's it, we are ready for UI testing, let's create `TestGoogle.java` file in test directory, it should look like this:
```java
package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestGoogle {

    @Test
    public void testGoogle() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Google", driver.getTitle());
        WebElement findLine = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input"));
        findLine.sendKeys("SQR course is the best");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[3]/center/input[1]"));
        searchButton.click();
        findLine = driver.findElement(By.xpath("/html/body/div[4]/form/div[2]/div[1]/div[2]/div/div[2]/input"));
        Assert.assertEquals("SQR course is the best", findLine.getAttribute("value"));
        driver.quit();
    }
}
```
That's it, we may run our test with `mvn test` and it should autotest Google webpage, we're done.

## Homework

As a homework you need to pick any website/application you want. Write website/app in your work, and check that works submitted(even without ready for review label) before you, your choice should be unique, one student - one application. After you've chosen your application to test, develop at least three explarotory test cases with complete description of what you've tested. Then you will need to fully automate at least one of the tests using Selenium. And push your code to the gitlab together with the link to your Exploratotry tests(or tests themselfs).

###Extra
I will put one more point if your Selenium test automation would be in gitlab ci and there would not be driver binary in repository

## Exploratory tours

We decided to UI test [acm.timus.ru](acm.timus.ru), a well known site for hosting competitive programming tasks.

**Note**: we decided to parse our tour's steps as a simple enumerated list since it's 2 am and we really don't wanna mess around markdown tables.

### Tour 1

**Goal**: test the "Online Judge" tab at the top of the home page.\
**Time**: ~11 minutes.

1. Open the main page. Expect the welcome page. OK.
2. Press the FAQ button. Expect the FAQ page. OK.
3. Press the news button. Expect the news page with several threads. OK.
4. Press the webboard button. Expect a table with several discussions and some options for formatting the table. OK.
5. Press the links button. Expect a list of related external links. OK.

### Tour 2

This is the test tour we are going to automate.

**Goal**: test the "Problems" tab at the top of the home page.\
**Time**: ~14 minutes.

1. Open the main page. Expect the welcome page. OK.
2. Press the problem set button. Expect a bunch of lists for various classes of problems. OK.
3. Press the all problems button. Expect a table of the complete problem set with some options for formatting the table. OK.
4. Press on one of the problems. Expect a problem description to appear with an option to submit a solution. OK.
5. Press on the submit solution button. Expect a prompt that asks for identification, language, problem, and source code to appear. OK.
6. Fill up the form and press submit. Expect to be forwarded to the status page with our solution appearing there. OK.

### Tour 3

**Goal**: test table formatting in the problem set.\
**Time**: 

1. Open the main page. Expect the welcome page. OK.
2. Open the problem set page. Expect a list of problems of various classes. OK.
3. Open the all problems tab. Expect a table of problems. OK.
4. Press on the "hide" button. Expect the previous solved A+B Problem to disappear. OK.
5. Press on "authors" button. Expect the problem table to be sorted by decreasing number of authors. OK.
6. Press on "difficulty" button. Expect the problem table to be sorted by difficulty. OK.