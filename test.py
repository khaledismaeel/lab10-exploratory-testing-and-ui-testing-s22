#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from selenium.webdriver.support.select import Select

class TestWebsite:
    # 1. Check browser configuration in browser_setup_and_teardown
    # 2. Run 'Selenium Tests' configuration
    # 3. Test report will be created in reports/ directory

    @pytest.fixture(autouse=True)
    def browser_setup_and_teardown(self):
        self.use_selenoid = False  # set to True to run tests with Selenoid

        if self.use_selenoid:
            self.browser = webdriver.Remote(
                command_executor='http://localhost:4444/wd/hub',
                desired_capabilities={
                    "browserName": "chrome",
                    "browserSize": "1920x1080"
                }
            )
        else:
            self.browser = webdriver.Chrome(
                executable_path=ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())

        self.browser.maximize_window()
        self.browser.implicitly_wait(10)
        self.browser.get("https://acm.timus.ru/")

        yield

        self.browser.close()
        self.browser.quit()


    def test_tour_2(self):
        # step 1
        welcome_phrase = 'Welcome to the Timus Online Judge'

        titles = self.browser.find_elements_by_class_name('title')
        welcome_phrase_found = False
        for title in titles:
            if welcome_phrase in title.text:
                welcome_phrase_found = True
        assert welcome_phrase_found

        # step 2
        problem_set_button = self.browser.find_element_by_link_text('Problem set')
        problem_set_button.click()
        assert self.browser.current_url == 'https://acm.timus.ru/problemset.aspx'

        problem_set_title = 'Problem set'
        problem_set_title_found = False
        titles = self.browser.find_elements_by_class_name('title')
        for title in titles:
            if problem_set_title in title.text:
                problem_set_title_found = True
        assert problem_set_title_found

        # step 3
        all_problems_button = self.browser.find_element_by_partial_link_text('All problems')
        all_problems_button.click()

        assert self.browser.current_url == 'https://acm.timus.ru/problemset.aspx?space=1&page=all'

        tables = self.browser.find_element_by_class_name('problemset')
        assert tables

        # step 4

        a_plus_b_problem_button = self.browser.find_element_by_partial_link_text('A+B Problem')
        a_plus_b_problem_button.click()

        assert self.browser.current_url == 'https://acm.timus.ru/problem.aspx?space=1&num=1000'

        problem_title = self.browser.find_element_by_class_name('problem_title')
        assert problem_title.text == '1000. A+B Problem'

        submit_solution_button = self.browser.find_element_by_link_text('Submit solution')
        assert submit_solution_button

        # step 5
        submit_solution_button.click()


        selects = self.browser.find_elements_by_tag_name('select')
        inputs = self.browser.find_elements_by_tag_name('input')
        textareas = self.browser.find_elements_by_tag_name('textarea')

        judge_id = None
        language = None
        problem = None
        source = None
        submit = None

        for element in inputs + selects + textareas:
            if not element.is_displayed():
                continue
            if element.get_attribute('name') == 'JudgeID':
                judge_id = element
            if element.get_attribute('name') == 'ProblemNum':
                problem = element
            if element.get_attribute('name') == 'Language':
                language = element
            if element.get_attribute('name') == 'Source':
                source = element
            if element.get_attribute('value') == 'Submit':
                submit = element

        assert judge_id
        assert language
        assert problem
        assert source

        # step 6

        with open('judge_id.txt') as file:
            my_judge_id = file.read().strip()

        judge_id.send_keys(my_judge_id)
        Select(language).select_by_visible_text('Python 3.8 x64')
        problem.send_keys('1000')
        source.send_keys('''
            inp = input().split(' ')
            a, b = int(inp[0]), int(inp[1])
            print(a + b)
        ''')

        submit.click()

        assert self.browser.current_url == 'https://acm.timus.ru/status.aspx?space=1'

